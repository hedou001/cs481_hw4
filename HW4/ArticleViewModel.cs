﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW4
{
    public class ArticleViewModel
    {
        public string ImageSource { get; set; }
        public string SmallImageSource { get; set; }
        public string Title { get; set; }
        public string SmallText { get; set; }
        public string Text { get; set; }
    }
}
