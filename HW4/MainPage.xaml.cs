﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private ObservableCollection<ArticleViewModel> articles = new ObservableCollection<ArticleViewModel>();
        private HashSet<ArticleViewModel> deletedArticles = new HashSet<ArticleViewModel>();
        private bool FirstTimeRefresh = true;
        public MainPage()
        {
            InitializeComponent();

            PopulateList();
            ArticleList.ItemsSource = articles;
        }

        // Event Management: PullToRefresh
        private void ArticleList_Refreshing(object sender, EventArgs e)
        {
            if (FirstTimeRefresh == true)
            {
                FirstTimeRefresh = false;
                articles.Add(new ArticleViewModel
                {
                    Title = "SLP Students Gain New Perspective From Guatemala Trip",
                    SmallText = "While her fellow Cal State San Marcos professors were knee deep in the first day of final examinations last December, Kristen Nahrstedt received a call asking if she could be ready in three weeks to take a group of speech-language pathology students to Guatemala for a fortnight.",
                    Text = "While her fellow Cal State San Marcos professors were knee deep in the first day of final examinations last December, Kristen Nahrstedt received a call asking if she could be ready in three weeks to take a group of speech-language pathology students to Guatemala for a fortnight.\n\n“Part of me was like, ‘Oh my gosh, I totally want to go,’ ” said Nahrstedt, who is a lecturer and has been the director of clinical education in the speech-language pathology department at CSUSM since 2013. “But then there was a part that was like, ‘What? In three weeks I’ve got to leave? What are you talking about?’ ”After a quick check-in with her personal responsibilities, Nahrstedt heeded the call. And it ended up being one of the best decisions of her professional career.\n\n“To be able to go over there and see the differences here in the states and see the amount of services we have for our population compared to other countries, especially a Third World country, it’s quite an eye-opening experience,” Nahrstedt said. “I think for a lot of these students, it kind of shapes their practice once they start to go out and become speech-language pathologists.\n\n”This is the first time CSUSM SLP students have done clinical work in a foreign country.\n\nThe person who laid the foundation for the trip – and who’s indirectly responsible for Nahrstedt’s attendance on the trip – is Darin Woolpert, an assistant professor at CSUSM whose expertise is working with bilingual kids in the field of SLP.",
                    SmallImageSource = "smallnarstedt.jpeg",
                    ImageSource = "narstedt.jpeg"
                });
            }
            foreach (ArticleViewModel article in deletedArticles)
            {
                articles.Add(article);
            }
            deletedArticles.Clear();
            ArticleList.IsRefreshing = false;
        }

        // Event Management: Item tapped (not selected just tapped)
        private void ArticleList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            // Access the Model to send it to the article page
            var article = e.Item as ArticleViewModel;
            Navigation.PushAsync(new ArticlePage(article));
        }

        //
        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            // Retrieving the selected article in the listview
            MenuItem menuItem = sender as MenuItem;
            var article = menuItem.BindingContext as ArticleViewModel;

            articles.Remove(article);
            deletedArticles.Add(article);
        }

        // Populate articles
        private void PopulateList()
        {
            articles.Add(new ArticleViewModel
            {
                Title = "Greek Life Continuing to Grow and Thrive on Campus",
                SmallText = "Nick Krueger considers himself lucky to have found a perfect fit when he pledged a fraternity at Cal State San Marcos in fall 2016.",
                Text = "Nick Krueger considers himself lucky to have found a perfect fit when he pledged a fraternity at Cal State San Marcos in fall 2016.\n\nIt was during Krueger’s second year on campus and he had just two options. Fortunately for Krueger, he felt at home with one of those choices and joined Sigma Chi.\n\nWhile Krueger is grateful that it worked out, he is glad current and future CSUSM students have more options to find a place that suits them – and those options are increasing.\n\nCSUSM is welcoming the fraternity Delta Sigma Phi and the sorority Gamma Phi Beta this semester, bringing the total number of Greek organizations on campus to 16. ",
                ImageSource = "gammaphibeta.jpeg",
                SmallImageSource = "smallgammaphibeta.jpeg"
            });
            articles.Add(new ArticleViewModel
            {
                Title = "Congressional Internship Solidifies Career Aspirations",
                SmallText = "Lauren Loeb has been interested in political science ever since visiting Washington, D.C., for the first time as an eighth grader.",
                Text = "Lauren Loeb has been interested in political science ever since visiting Washington, D.C., for the first time as an eighth grader.\n\nLoeb had an opportunity to return to Washington last fall, this time for a nearly three-month stint as a participant in the Panetta Institute Congressional Internship Program. It was an experience that affirmed Loeb’s desire to pursue a career in law or politics.\n\n“I’ve had a pretty good idea of what I wanted to do for a while,” Loeb said. “But I think this experience just gave me the confidence to be able to know that it is something that I can achieve, and it also laid out the path that I think I need to take to get there.\n\n”Loeb, who is majoring in political science with a minor in history, spent her first two years of college at Cal State San Marcos and was the university’s representative for the Panetta internship last fall. Though she transferred to Cal State Fullerton this spring to cut down on living expenses, she returned to CSUSM earlier this month to sit on the interview committee that will select a candidate for the program next fall.\n\nLoeb was among 26 students who participated in the program, which annually selects one student from each California State University campus, Dominican University of California, Saint Mary’s College and Santa Clara University.\n\nThe internship began with a two-week prep period in Monterey at the Panetta Institute, which was founded in 1997 by former CIA Director and Secretary of Defense Leon Panetta and his wife, Sylvia. Then it was off to Washington for a 2½-month assignment working on Capitol Hill. The Panetta Institute covers all program costs, and interns receive 20 semester units.",
                ImageSource = "laurenloeb.jpg",
                SmallImageSource = "smalllaurenloeb.jpg"
            });
            articles.Add(new ArticleViewModel
            {
                Title = "Zombies, Vampires and Witches as a New Kind of Hero",
                SmallText = "Cal State San Marcos professor Natalie Wilson wants us to look at zombies, vampires and witches as warriors for social justice.",
                Text = "Cal State San Marcos professor Natalie Wilson wants us to look at zombies, vampires and witches as warriors for social justice.\n\nWilson, who teaches primarily for the Women’s, Gender and Sexuality Studies Department and who has written extensively about horror in popular culture, has released a new book, “Willful Monstrosity: Gender and Race in 21st Century Horror,” examining characters in the current horror renaissance as metaphors for the battle against sexual violence, greed, police brutality and other social justice issues. Her work focuses on productions such as “Get Out,” “Us,” “Chilling Adventures of Sabrina” and “Stranger Things,” and works by authors such as Carmen Maria Machado, Justin Cronin and Helen Oyeyemi (follow her blog where she posts regularly on horror).\n\n“Horror stories today specifically speak to things that are happening right now, including police brutality and the #MeToo movement,” she said. “Monsters can be seen as heroic figures. Even where monsters are not great, humans are worse.\n\n”Wilson said she wrote “Willful Monstrosity” partly as a counterpoint to her last book, “Seduced by Twilight,” which critiqued the “Twilight” saga from a feminist perspective and highlighted the worrying message that the series sent to the teens who comprised its biggest audience: It’s OK to stay with an abusive partner.\n\n“In this book, I wanted to look at the good or positive characteristics that monsters bring,” Wilson said.“Us” offers a metaphor about oppressed workers and the underclass. “Get Out” illuminates white supremacy and black exploitation through its story detailing a white community’s practice of placing the brains of white people in black bodies.",
                ImageSource = "nataliewilson.jpg",
                SmallImageSource = "smallnataliewilson.jpg"
            });
        }
    }
}
