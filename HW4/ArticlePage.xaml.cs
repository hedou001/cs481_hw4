﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW4
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArticlePage : ContentPage
    {
        public ArticlePage(ArticleViewModel article)
        {
            InitializeComponent();

            // Added a binding context for the XAML front
            this.BindingContext = article;
        }

        private void Back_Clicked(object sender, EventArgs e)
        {
            // Back to article list
            Navigation.PopAsync();
        }

        private void Home_Clicked(object sender, EventArgs e)
        {
            // Back to article list
            Navigation.PopToRootAsync();
        }
    }
}